package cs3500.hw5;

import java.awt.Color;

/**
 * Square object. Has a width and a height as its class specific dimensions.
 */
public class Rectangle extends AbstractShape {

  private int width;
  private int height;


  /**
   * Constructor.
   * @param name        name of the object
   * @param placement   reference point for placement of object
   * @param color       color
   * @param posx        x position
   * @param posy        y position
   * @param width       width of the rectangle
   * @param height      height of the rectangle
   */
  public Rectangle(String name, String placement, Color color, int posx,
      int posy, int width, int height) {
    super(name, placement, color, posx, posy);
    this.width = width;
    this.height = height;

  }

  /**
   * Gets the class name for the models hashmap.
   * @return the class name.
   */
  @Override
  public String getClassName() {
    return "Rectangle";
  }

  /**
   * Gets the dimension of the square in the print format.
   * @return the dimension of the square
   */
  @Override
  public String getSpecificDim() {
    return "Width: " + Integer.toString(width) + ", Height: " + Integer.toString(height);
  }

  /**
   * Sets the dimensions for a move operation.
   * @param dimtype the dimension that will be changed
   * @param dim     the value the dimension will be changed to
   */
  @Override
  public void setDim(String dimtype, int dim) {
    if (dimtype.equals("Width")) {
      this.width = dim;

    }
    else if (dimtype.equals("Height")) {
      this.height = dim;
    }

    else {
      throw new IllegalArgumentException("Entered Invalid Field");
    }
  }

  /**
   * Prints the wight and height of the rectangle.
   * @return the dimensions of teh rectangle.
   */
  @Override
  public String printDimensions() {
    String output = "Width: " + Double.toString(width) + ", Height: "
        + Double.toString(height);
    return output;
  }
}
