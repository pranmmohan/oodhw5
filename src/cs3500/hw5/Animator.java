package cs3500.hw5;

import java.awt.Color;

/**
 * This interface of the Animation model.
 */
public interface Animator {


  /**
   * Moves the shape during a specific time frame.
   * @param name  name of the shape
   * @param destx final x coord of the shape
   * @param desty final y coord of the shape
   * @param tFrom start time
   * @param tTo   end time
   * @return before and after
   */
  String move(String name, double destx, double desty, int tFrom, int tTo);

  /**
   * Changes the color of the shape.
   * @param name  name of the shape
   * @param color color to change the shape
   * @param tFrom start time
   * @param tTo   end time
   * @return before and after.
   */
  String changeColor(String name, Color color, int tFrom, int tTo);


  /**
   * Scales a dimension of the shape.
   * @param name        name of the shape
   * @param attribute   attribute of the shape to change
   * @param value       value of the dimension to change
   * @param tFrom       start time
   * @param tTo         end time
   * @return before and after
   */
  String scales(String name, String attribute, int value,  int tFrom, int tTo);

  /**
   * Prints the shape.
   * @param name name of the shape to print
   * @return the details of the shape
   */
  String printShape(String name);


  /**
   * Sets the time the shape appears and ends.
   * @param name  name of the shape changing
   * @param tFrom time when the shape appears
   * @param tTo   time when the shape disappears
   */
  void setTime(String name, int tFrom, int tTo);






}
