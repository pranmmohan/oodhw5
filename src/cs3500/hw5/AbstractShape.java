package cs3500.hw5;

import java.awt.Color;

/**
 * AbstractShape class for all shapes.
 */
public abstract class AbstractShape {

  protected String name;
  protected String placement;
  protected Color color;
  protected double posx;
  protected double posy;
  protected int tAppear;
  protected int tDisappear;

  /**
   * Super constructor.
   * @param name        name of the shape.
   * @param placement   reference to the placement.
   * @param color       color of the shape
   * @param posx        x position of the shape
   * @param posy        y position of the shape
   */
  public AbstractShape(String name, String placement, Color color, double posx, double posy) {
    this.name = name;
    this.placement = placement;
    this.color = color;
    this.posx = posx;
    this.posy = posy;
    this.tAppear = 0;
    this.tDisappear = 0;
  }

  /**
   * Set the time of the shape.
   * @param tAppear     time when the shape appears.
   * @param tDisappear  time when the shape disappears.
   */
  public void setTime(int tAppear, int tDisappear) {
    this.tAppear = tAppear;
    this.tDisappear = tDisappear;

  }

  /**
   * Prints the shape's details.
   * @return the shape.
   */
  @Override
  public String toString() {
    String output = "Name: " + this.name + "\n"
        +  "Type: " + this.getClassName() +  "\n"
        +  "Lower-Left " + placement + ": " +  this.printcoords() + ", " + this.getSpecificDim()
        +  ", Color: " + printcolors() + "\n"
        +  "Appears at t=" + Integer.toString(tAppear) + "\n"
        +  "Disappears at t=" + Integer.toString(tDisappear);

    return output;
  }


  /**
   * Prints the coordinates of the shape.
   * @return coordinates
   */
  public String printcoords() {
    return "(" + Double.toString(posx)  + "," + Double.toString(posy) + ")";
  }

  /**
   * Prints the colors of the shape.
   * @return colors in (r,g,b) format
   */
  public String printcolors() {
    return  "("
        + Integer.toString(color.getRed()) + ","
        + Integer.toString(color.getGreen()) + ","
        + Integer.toString(color.getBlue()) + ")";
  }

  /**
   * Gets the name of the shape.
   * @return name of the shape
   */
  public abstract String getClassName();


  /**
   * Gets the unique dimensions of the shape.
   * @return the dimensions of the shape
   */
  public abstract String getSpecificDim();


  /**
   * Gets the name of the name of the shape.
   * @return name of the shape
   */
  public String getName() {
    return this.name;
  }

  /**
   * Sets the color of the shape.
   * @param color new color of the shape
   */
  public void setColor(Color color) {
    this.color = color;
  }

  /**
   * Sets the new position of the shape.
   * @param posx x position of the shape
   * @param posy y position of the shape
   */
  public void setMove(double posx, double posy) {
    this.posx = posx;
    this.posy  = posy;
  }

  /**
   * Sets the new dimension of the shape.
   * @param dimtype the dimension to be changed
   * @param dim the value the dimension is going to be changed to.
   */
  public abstract void setDim(String dimtype, int dim);


  /**
   * Prints the dimensions of the shape.
   * @return the dimensions of the shape.
   */
  public abstract String printDimensions();

  /**
   * Checks if the time to move the shape is valid.
   * @param tFrom time the shape starts moving at
   * @param tTo   time the shape starts moving till
   * @return      valid times
   */
  public boolean validTime(int tFrom, int tTo) {
    return this.tAppear > tFrom | this.tDisappear < tTo;
  }












}
