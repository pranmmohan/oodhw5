package cs3500.hw5;

import java.awt.Color;

/**
 * Oval class. It is a shape.
 */
public class Oval extends AbstractShape {

  double radiusx;
  double radiusy;


  /**
   * Constructor.
   * @param name        name of the shape
   * @param placement   reference point for position
   * @param color       color
   * @param posx        x coordinate
   * @param posy        y coordinate
   * @param radiusx     x radius
   * @param radiusy     y radius
   */
  public Oval(String name, String placement, Color color, int posx, int posy,
      double radiusx, double radiusy) {
    super(name, placement, color, posx, posy);
    this.radiusx = radiusx;
    this.radiusy = radiusy;

  }

  /**
   * Prints the class name.
   * @return the class name.
   */
  @Override
  public String getClassName() {
    return "Oval";
  }

  /**
   * Prints the dimension of the shape.
   * @return the dimension of the shape.
   */
  @Override
  public String getSpecificDim() {
    return "X radius: " + Double.toString(radiusx) + ", Y radius: " + Double.toString(radiusy);
  }

  /**
   * Sets one of the dimensions to the given value.
   * @param dimtype the dimension to be changed
   * @param dim the value the dimension is going to be changed to.
   */
  @Override
  public void setDim(String dimtype, int dim) {
    if (dimtype.equals("X Radius")) {
      this.radiusx = dim;

    }
    else if (dimtype.equals("Y Radius")) {
      this.radiusy = dim;
    }

    else {
      throw new IllegalArgumentException("Entered Invalid Field");
    }
  }

  /**
   * Prints the dimensions of the oval its xradius and yradius.cannersca
   * @return the dimensions in string format
   */
  @Override
  public String printDimensions() {
    String output = "X Radius: " + Double.toString(radiusx) + ", Y Radius: "
        + Double.toString(radiusy);
    return output;
  }
}
