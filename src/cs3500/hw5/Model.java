package cs3500.hw5;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * The Model class. It has animation functions.
 */
public class Model implements Animator {


  List<AbstractShape> shapes;
  HashMap<String, AbstractShape> nametoShape;


  public Model() {
    shapes = new ArrayList<>();
    nametoShape = new HashMap<>();
  }


  /**
   * Constructor for the model class.
   * @param shapes list of shapes that this model will work on
   */
  public Model(List<AbstractShape> shapes) {
    this.shapes = shapes;
    for (AbstractShape shape: shapes) {
      nametoShape.put(shape.getName(), shape);
    }
  }

  /**
   * Adds a ashape to the list of the shapes in the model.
   * @param shape to be added to the model
   */
  public void addShape(AbstractShape shape) {
    this.shapes.add(shape);
    nametoShape.put(shape.getName(), shape);
  }

  /**
   * Sets the time of the shape as to when it appears and disappears.
   * @param name  name of the shape changing
   * @param tFrom time when the shape appears
   * @param tTo   time when the shape disappears
   */
  public void setTime(String name, int tFrom, int tTo) {
    if (this.nametoShape.get(name) == null) {
      throw new IllegalArgumentException("Shape doesnt exist");
    }
    if ( tFrom > tTo | tTo <  0 | tFrom < 0) {
      throw new IllegalArgumentException("Bad times");
    }
    nametoShape.get(name).setTime(tFrom, tTo);
  }




  /**
   * Moves the shape.
   * @param name  name of the shape
   * @param destx final x coord of the shape
   * @param desty final y coord of the shape
   * @param tFrom start time
   * @param tTo   end time
   * @return before and after
   */

  @Override
  public String move(String name, double destx, double desty, int tFrom, int tTo) {

    checkTimes(tFrom, tTo, name);
    AbstractShape s = nametoShape.get(name);
    String output = "Shape " + s.getName() + " moves from " + s.printcoords()
        + " to ";
    nametoShape.get(name).setMove(destx, desty);
    output = output + s.printcoords() + " from t=" + Integer.toString(tFrom) + " to "
        + "t=" + Integer.toString(tTo);

    return output;
  }

  /**
   * Changes the color of the shape.
   * @param name  name of the shape
   * @param color color to change the shape
   * @param tFrom start time
   * @param tTo   end time
   * @return color efore and after
   */
  @Override
  public String changeColor(String name, Color color, int tFrom, int tTo) {

    checkTimes(tFrom, tTo, name);
    AbstractShape s = nametoShape.get(name);
    String output = "Shape " + s.getName() + " changes color from " + s.printcolors()
        + " to ";
    nametoShape.get(name).setColor(color);
    output = output + s.printcolors() + " from t=" + Integer.toString(tFrom) + " to "
        + "t=" + Integer.toString(tTo);

    return output;

  }

  /**
   * Checks if the shape can be animated between the given times.
   * @param tFrom when the shape starts its animation
   * @param tTo   when the shape ends its animation
   * @param name  shape that is being animated
   */
  private void checkTimes(int tFrom, int tTo, String name) {
    if (tFrom > tTo | tFrom < 0 | tTo < 0 || nametoShape.get(name).validTime(tFrom, tTo)) {
      throw new IllegalArgumentException("Invalid times");
    }
  }


  /**
   * Changes the size of the shape.
   * @param name        name of the shape
   * @param attribute   attribute of the shape to change
   * @param value       value of the dimension to change
   * @param tFrom       start time
   * @param tTo         end time
   * @return size before and after
   */
  @Override
  public String scales(String name, String attribute, int value, int tFrom, int tTo) {

    checkTimes(tFrom, tTo, name);

    AbstractShape s = nametoShape.get(name);
    String output = "Shape " + s.getName() + " changes from " + s.printDimensions()
        + " to ";
    nametoShape.get(name).setDim(attribute, value);
    output = output + s.printDimensions() + " from t=" + Integer.toString(tFrom) + " to "
        + "t=" + Integer.toString(tTo);

    return output;

  }

  /**
   * Prints the shape.
   * @param name name of the shape to print
   * @return details of the shape
   */
  @Override
  public String printShape(String name) {

    if (nametoShape.get(name) == null) {
      throw new IllegalArgumentException("This shape does not exist");
    }
    return nametoShape.get(name).toString();
  }
}
