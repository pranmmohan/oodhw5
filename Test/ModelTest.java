package cs3500.hw5;

import static org.junit.Assert.assertEquals;

import java.awt.Color;
import org.junit.Before;
import org.junit.Test;

/**
 * All the tests for the model class.
 * Also contains the test for
 */
public class ModelTest {


  Model model = new Model();


  @Before
  public void initialize() {
    model = new Model();
  }

  @Test
  public void addShape() {
    model.addShape(new Rectangle("r", "bottom left",
        new Color(1, 2, 3), 50, 50, 4, 8));
    String output = "Name: r\n"
        + "Type: Rectangle\n"
        + "Lower-Left bottom left: (50.0,50.0), Width: 4, Height: 8, Color: (1,2,3)\n"
        + "Appears at t=0\n"
        + "Disappears at t=0";
    assertEquals(output, model.printShape("r"));

  }

  @Test
  public void printShape() {
    model.addShape(new Oval("js", "center",
        new Color(23, 2,2), 50 , 70, 10, 10));

    model.setTime("js", 30, 100);

    String output = "Name: js\n"
        + "Type: Oval\n"
        + "Lower-Left center: (50.0,70.0), X radius: 10.0, Y radius: 10.0, Color: (23,2,2)\n"
        + "Appears at t=30\n"
        + "Disappears at t=100";
    assertEquals(output, model.printShape("js"));
  }

  @Test
  public void setTime() {
    model.addShape(new Rectangle("r", "bottom left",
        new Color(1, 2, 3), 50, 50, 4, 8));
    model.setTime("r", 4, 80);
    String output = "Name: r\n"
        + "Type: Rectangle\n"
        + "Lower-Left bottom left: (50.0,50.0), Width: 4, Height: 8, Color: (1,2,3)\n"
        + "Appears at t=4\n"
        + "Disappears at t=80";
    assertEquals(output, model.printShape("r"));
  }

  @Test
  public void move() {
    model.addShape(new Rectangle("r", "bottom left",
        new Color(1, 2, 3), 50, 50, 4, 8));
    model.setTime("r", 4, 80);
    String output = "Shape r moves from (50.0,50.0) to (500.0,300.0) from t=20 to t=25";
    assertEquals(output, model.move("r", 500, 300, 20, 25));
  }

  @Test
  public void scales() {
    model.addShape(new Rectangle("r", "bottom left",
        new Color(1, 2, 3), 50, 50, 4, 8));
    model.setTime("r", 4, 50);
    String output = "Shape r changes from Width: 4.0, Height: 8.0 to Width: 500.0, Height: 8.0 "
        + "from t=10 to t=30";
    assertEquals(output, model.scales("r","Width", 500, 10, 30));
  }

  @Test(expected = IllegalArgumentException.class)
  public void badscales() {
    model.addShape(new Rectangle("r", "bottom left",
        new Color(1, 2, 3), 50, 50, 4, 8));
    model.setTime("r", 4, 50);
    model.scales("r","Radius", 500, 10, 30);
  }

  @Test
  public void changeColor() {
    model.addShape(new Oval("j", "center",
        new Color(32, 3, 4), 100, 300, 8, 10));
    model.setTime("j", 4, 80);
    String output = "Shape j changes color from (32,3,4) to (0,0,0) from t=7 to t=10";
    assertEquals(output, model.changeColor("j", new Color(0, 0, 0), 7, 10));
  }



  @Test(expected = IllegalArgumentException.class)
  public void noShape() {
    model.addShape(new Rectangle("r", "bottom left",
        new Color(1, 2, 3), 50, 50, 4, 8));
    model.printShape("j");
  }


  @Test(expected = IllegalArgumentException.class)
  public void badMove() {
    model.addShape(new Rectangle("r", "bottom left",
        new Color(1, 2, 3), 50, 50, 4, 8));
    model.move("r", 500.0, 500.0, -1, 4);
  }

  @Test(expected = IllegalArgumentException.class)
  public void badChangeColor() {
    model.addShape(new Rectangle("r", "bottom left",
        new Color(1, 2, 3), 50, 50, 4, 8));
    model.move("r", 50.3, 4, -1, 3);
  }

  @Test(expected = IllegalArgumentException.class)
  public void badResize() {
    model.addShape(new Rectangle("r", "bottom left",
        new Color(1, 2, 3), 50, 50, 4, 8));
    model.move("r", 3.3, 54, 0, -60);

  }

  @Test(expected = IllegalArgumentException.class)
  public void badChangetime() {
    model.setTime("r", 50, 60);
  }

  @Test(expected = IllegalArgumentException.class)
  public void badtimes() {

    model.addShape(new Rectangle("r", "bottom left",
        new Color(1, 2, 3), 50, 50, 4, 8));
    model.setTime("r", 50, 30);
  }



}