There are many shapes, and all shapes have common
variables such as time they appear, time they disappear,
color, name. So the ideal choice is to create an abstract
class that allows shapes to use those same properties as
well as add properties unique to the shape like their specific
dimensions. The model extends the animator interface which
includes all the types of animations so far. This seems
better than my initial idea of using an abstract class
of animations because the model can directly animate
the shape based on the input from the controller,
instead of creating new shapes.t